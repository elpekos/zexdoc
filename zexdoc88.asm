;       Minimal Z88 for ZEXDOC exerciser
;
;       T.Peycru, 2019
;
;       we're in ROM, copy and execute ZEDOC in RAM
;       on reset, bank 0 is bound to segment 0-3
;

defc    zd_org  = $0000

org     $0000

        di                                      ; disable interrupts
        jp      start | $C000                   ; bank 0 in segment 3

.zd_bin binary  "./zexdoc.bin"
.zd_end

.start
        ld      a, @00000101                    ; RAMS | LCDON
        out     ($B0), a                        ; COM register
        ld      a, $21                          ; upper 8K of bank $20
        out     ($D0), a                        ; segment 0 is RAM
        ld      sp, $3FFE                       ; stack at end of segment 0

        ld      hl, zd_bin | $C000              ; from segment 3
        ld      de, zd_org                      ; to segment 0
        ld      bc, zd_end - zd_org
        ldir                                    ; copy ZEXDOC at $0000
        jp      zd_org                          ; execute it
