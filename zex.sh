#!/bin/bash

# zexdoc compilation script
# T.Peycru 2019

# directory cleanup
rm -f *.bin
rm -f *.rom
rm -f *.map

# generate binary for Z88 blink
mpm -b -DZ88 ./zexdoc.asm
mpm -b ./zexdoc88.asm
z88card -f ./zexdoc88.ldm

# generate binary for Z88 BBC Basic
rm -f ./zexdoc.bin
mpm -b -DZ88 -DBASIC ./zexdoc.asm
mv -f ./zexdoc.bin ./zexbbc.bin

# generate binaries for FPGA board
rm -f ./zexdoc.bin
mpm -b ./zexdoc.asm
srec_cat zexdoc.bin -binary -o zexdoc.mem -mem 8
srec_cat zexdoc.bin -binary -o zexdoc.mif -mif 8

