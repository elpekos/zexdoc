; ZEXDOC, Z80 documented instruction set exerciser
;
; Minimal Z80 port (CPU and 64K RAM)
;
; T. Peycru, 2019
;
;
; zexdoc.src - Z80 documented instruction set exerciser
; Original Copyright (C) 1994  Frank D. Cringle
;
; This program is free software; you can redistribute it and/or
; modify it under the terms of the GNU General Public License
; as published by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

        org     0000h

        di
        ld      sp, $3FFE       ; set stack
IF Z88
        xor     a               ; no interrupt
        out     ($B1), a        ; set INT enable
        ld      a, @00000111    ; 38400 bauds
        out     ($E4), a        ; set TX control
ENDIF
IF BASIC
        ld      a, @00000100    ; RAMS (LCD off)
        out     ($B0), a        ; COM register
        ld      a, $21          ; upper 8K of bank $20
        out     ($D0), a        ; segment 0 is RAM
        ld      hl, 16384       ; from segment 1
        ld      de, 0           ; to segment 0
        ld      bc, 16384       ; one bank
        ldir                    ; copy
        jp      intos0
.intos0
        inc     a               ; $22
        out     ($D1), a        ; S1
        inc     a               ; $23
        out     ($D2), a        ; S2
        inc     a               ; $24
        out     ($D3), a        ; S3, 64K RAM
ENDIF
        jp      entry

        defs    ($0100-$PC) $00 ; fill with NOP to start like CPM

;       entry at $0100 on CPM

.entry
        jp      start

;       machine state before test (needs to be at predictably constant address)

.msbt   defs    14
.spbt   defs    2

        defc    msbthi  = msbt / 0100h
        defc    msbtlo  = msbt & 0ffh

; For the purposes of this test program, the machine state consists of:
;       a 2 byte memory operand, followed by
;       the registers iy,ix,hl,de,bc,af,sp
; for a total of 16 bytes.

; The program tests instructions (or groups of similar instructions)
; by cycling through a sequence of machine states, executing the test
; instruction for each one and running a 32-bit crc over the resulting
; machine states.  At the end of the sequence the crc is compared to
; an expected value that was found empirically on a real Z80.

; A test case is defined by a descriptor which consists of:
;       a flag mask byte,
;       the base case,
;       the incement vector,
;       the shift vector,
;       the expected crc,
;       a short descriptive message.
;
; The flag mask byte is used to prevent undefined flag bits from
; influencing the results.  Documented flags are as per Mostek Z80
; Technical Manual.
;
; The next three parts of the descriptor are 20 byte vectors
; corresponding to a 4 byte instruction and a 16 byte machine state.
; The first part is the base case, which is the first test case of
; the sequence.  This base is then modified according to the next 2
; vectors.  Each 1 bit in the increment vector specifies a bit to be
; cycled in the form of a binary counter.  For instance, if the byte
; corresponding to the accumulator is set to 0ffh in the increment
; vector, the test will be repeated for all 256 values of the
; accumulator.  Note that 1 bits don"t have to be contiguous.  The
; number of test cases "caused" by the increment vector is equal to
; 2^(number of 1 bits).  The shift vector is similar, but specifies a
; set of bits in the test case that are to be successively inverted.
; Thus the shift vector "causes" a number of test cases equal to the
; number of 1 bits in it.

; The total number of test cases is the product of those caused by the
; counter and shift vectors and can easily become unweildy.  Each
; individual test case can take a few milliseconds to execute, due to
; the overhead of test setup and crc calculation, so test design is a
; compromise between coverage and execution time.

; This program is designed to detect differences between
; implementations and is not ideal for diagnosing the causes of any
; discrepancies.  However, provided a reference implementation (or
; real system) is available, a failing test case can be isolated by
; hand using a binary search of the test space.

start:  ld      de,msg1
        call    strout

        ld      hl,tests        ; first test case
loop:   ld      a,(hl)          ; end of list ?
        inc     hl
        or      (hl)
        jp      z,done
        dec     hl
        call    stt
        jp      loop
        
done:   ld      de,msg2
        call    strout
        halt                    ; freeze
        jr      $PC

tests:
        ;defw    srz80,0
        ;defw    rotz80,0
        ;defw    ld8rr,0
        ;defw    ld8imx,0
        ;defw     bitx
        ;defw     bitz80,0
        ;defw    cpi1,0
        ;defw    srz80,0;
        ;defw    ld8imx,0
        ;defw    alu8i,0
        ;defw    ld8bd
        ;defw    bitx,0

        defw    ldixhr, ldiyhr, ldixlr, ldiylr          ; LD XYH/L,r
        defw    ldrixh, ldriyh, ldrixl, ldriyl          ; LD r,XYH/L
        defw    ldrixd, ldriyd, ldixdr, ldiydr          ; LD r,(XY+d) / LD (XY+d),r
        defw    ldxbr , ldxcr , ldxdr , ldxer , ldxar   ; illegal IX opcodes
        defw    ld8rrx                                  ; all previous in one

        defw    cpd1a, cpd1b ; fixed and splitted cpd1
        defw    tdaa, tdaah, tcpl, tscf, tccf ; splitted daaop and daa without H

        defw    adc16
        defw    add16
        defw    add16x
        defw    add16y
        defw    alu8i
        defw    alu8r
        defw    alu8rx
        defw    alu8x
        defw    bitx
        defw    bitz80
        ;defw    cpd1
        defw    cpi1
        ;defw    daaop
        defw    inca
        defw    incb
        defw    incbc
        defw    incc
        defw    incd
        defw    incde
        defw    ince
        defw    inch
        defw    inchl
        defw    incix
        defw    inciy
        defw    incl
        defw    incm
        defw    incsp
        defw    incx
        defw    incxh
        defw    incxl
        defw    incyh
        defw    incyl
        defw    ld161
        defw    ld162
        defw    ld163
        defw    ld164
        defw    ld165
        defw    ld166
        defw    ld167
        defw    ld168
        defw    ld16im
        defw    ld16ix
        defw    ld8bd
        defw    ld8im
        defw    ld8imx
        defw    ld8ix1
        defw    ld8ix2
        defw    ld8ix3
        defw    ld8ixy
        defw    ld8rr
        ;defw    ld8rrx
        defw    lda
        defw    ldd1
        defw    ldd2
        defw    ldi1
        defw    ldi2
        defw    negop
        defw    rldop
        defw    rot8080
        defw    rotxy
        defw    rotz80
        defw    srz80
        defw    srzx
        defw    st8ix1
        defw    st8ix2
        defw    st8ix3
        defw    stabd
        defw    0

;       ----

tstr    macro   (insn1,insn2,insn3,insn4,memop,riy,rix,rhl,rde,rbc,flags,acc,rsp)
.\lab_tr
        defb    \insn1,\insn2,\insn3,\insn4
        defw    \memop,\riy,\rix,\rhl,\rde,\rbc
        defb    \flags
        defb    \acc
        defw    \rsp
        if      $PC-\lab_tr <> 20
        error   "missing parameter"
        endif
        endm

;       ----

;       New tests, september 2019

ldixdr: defb    0d7h            ; flag mask
        tstr    0ddh,070h,1,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $C2,$97,$4D,$A0
        defm    "ld (ix+1),r...................",0

ldiydr: defb    0d7h            ; flag mask
        tstr    0fdh,070h,1,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $C2,$97,$4D,$A0
        defm    "ld (iy+1),r...................",0

ldxbr:  defb    0d7h            ; flag mask
        tstr    0ddh,040h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $EC,$44,$71,$E9
        defm    "ld ixb,r..(illegal)...........",0

ldxcr:  defb    0d7h            ; flag mask
        tstr    0ddh,048h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $6D,$6D,$75,$11
        defm    "ld ixc,r..(illegal)...........",0

ldxdr:  defb    0d7h            ; flag mask
        tstr    0ddh,050h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $F9,$96,$D8,$35
        defm    "ld ixd,r..(illegal)...........",0

ldxer:  defb    0d7h            ; flag mask
        tstr    0ddh,058h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $9E,$40,$3D,$81
        defm    "ld ixe,r..(illegal)...........",0

ldxar:  defb    0d7h            ; flag mask
        tstr    0ddh,078h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $8D,$C5,$08,$FA
        defm    "ld ixa,r..(illegal)...........",0

ldrixh: defb    0d7h            ; flag mask
        tstr    0ddh,044h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $38,$06,$9C,$8D
        defm    "ld r,ixh......................",0

ldriyh: defb    0d7h            ; flag mask
        tstr    0fdh,044h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $9B,$57,$6A,$83
        defm    "ld r,iyh......................",0

ldrixl: defb    0d7h            ; flag mask
        tstr    0ddh,045h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $A7,$7D,$A3,$B7
        defm    "ld r,ixl......................",0

ldriyl: defb    0d7h            ; flag mask
        tstr    0fdh,045h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $00,$A4,$24,$CF
        defm    "ld r,iyl......................",0

ldixhr: defb    0d7h            ; flag mask
        tstr    0ddh,060h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $39,$C9,$DF,$B4
        defm    "ld ixh,r......................",0

ldiyhr: defb    0d7h            ; flag mask
        tstr    0fdh,060h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $D5,$B2,$98,$E7
        defm    "ld iyh,r......................",0

ldixlr: defb    0d7h            ; flag mask
        tstr    0ddh,068h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $88,$A4,$F9,$71
        defm    "ld ixl,r......................",0

ldiylr: defb    0d7h            ; flag mask
        tstr    0fdh,068h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00000111,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $F9,$10,$C9,$D3
        defm    "ld iyl,r......................",0

ldrixd: defb    0d7h            ; flag mask
        tstr    0ddh,046h,1,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $3D,$E5,$A8,$E4
        defm    "ld r,(ix+1)...................",0

ldriyd: defb    0d7h            ; flag mask
        tstr    0fdh,046h,1,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    0,@00111000,0,0,0,0,0,0,0,0,0,0,0
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    $3D,$E5,$A8,$E4
        defm    "ld r,(iy+1)...................",0

;       ----

;<cpd> fixed
cpd1a:  defb    0d7h            ; flag mask
        tstr    0edh,0a9h,0,0,0c7b6h,072b4h,018f6h,msbt+8,08dbdh,1,0c0h,030h,094a3h
        tstr    0,0,0,0,0,0,0,0,0,010,0,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    $F8,$89,$D9,$EC
        defm    "<cpd>.........................",0

;<cpdr> fixed
cpd1b:  defb    0d7h            ; flag mask
        tstr    0edh,0b9h,0,0,0c7b6h,072b4h,018f6h,msbt+8,08dbdh,1,0c0h,030h,094a3h
        tstr    0,0,0,0,0,0,0,0,0,010,0,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    $EE,$6A,$B9,$43
        defm    "<cpdr>........................",0

; <daa>
tdaa:   defb    0d7h            ; flag mask
        tstr    027h,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    $A4,$61,$15,$58
        defm    "<daa>.........................",0

; <daah> without half-carry 
tdaah:  defb    @11000111                               ; SZ___PNC flag mask
        tstr    027h,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    $0F,$28,$8B,$01
        defm    "<daa/h>.......................",0

; <cpl>
tcpl:   defb    0d7h            ; flag mask
        tstr    02fh,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    $A2,$21,$5A,$02
        defm    "<cpl>.........................",0

; <scf>
tscf:   defb    0d7h            ; flag mask
        tstr    037h,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0 
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    $01,$DF,$B7,$7C
        defm    "<scf>.........................",0

; <ccf>
tccf:   defb    0d7h            ; flag mask
        tstr    03fh,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    $08,$D2,$0F,$E0
        defm    "<ccf>.........................",0

;       ----

; <adc,sbc> hl,<bc,de,hl,sp> (38,912 cycles)
adc16:  defb    0c7h            ; flag mask
        tstr    0edh,042h,0,0,0832ch,04f88h,0f22bh,0b339h,07e1fh,01563h,0d3h,089h,0465eh
        tstr    0,038h,0,0,0,0,0,0f821h,0,0,0,0,0       ; (1024 cycles)
        tstr    0,0,0,0,0,0,0,-1,-1,-1,0d7h,0,-1        ; (38 cycles)
        defb    0f8h,0b4h,0eah,0a9h                     ; expected crc
        defm    "<adc,sbc> hl,<bc,de,hl,sp>....",0

; add hl,<bc,de,hl,sp> (19,456 cycles)
add16:  defb    0c7h            ; flag mask
        tstr    9,0,0,0,0c4a5h,0c4c7h,0d226h,0a050h,058eah,08566h,0c6h,0deh,09bc9h
        tstr    030h,0,0,0,0,0,0,0f821h,0,0,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,0,0,-1,-1,-1,0d7h,0,-1        ; (38 cycles)
        defb    089h,0fdh,0b6h,035h                     ; expected crc
        defm    "add hl,<bc,de,hl,sp>..........",0

; add ix,<bc,de,ix,sp> (19,456 cycles)
add16x: defb    0c7h            ; flag mask
        tstr    0ddh,9,0,0,0ddach,0c294h,0635bh,033d3h,06a76h,0fa20h,094h,068h,036f5h
        tstr    0,030h,0,0,0,0,0f821h,0,0,0,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,0,-1,0,-1,-1,0d7h,0,-1        ; (38 cycles)
        defb    0c1h,033h,079h,00bh                     ; expected crc
        defm    "add ix,<bc,de,ix,sp>..........",0

; add iy,<bc,de,iy,sp> (19,456 cycles)
add16y: defb    0c7h            ; flag mask
        tstr    0fdh,9,0,0,0c7c2h,0f407h,051c1h,03e96h,00bf4h,0510fh,092h,01eh,071eah
        tstr    0,030h,0,0,0,0f821h,0,0,0,0,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,-1,0,0,-1,-1,0d7h,0,-1        ; (38 cycles)
        defb    0e8h,081h,07bh,09eh                     ; expected crc
        defm    "add iy,<bc,de,iy,sp>..........",0

; aluop a,nn (28,672 cycles)
alu8i:  defb    0d7h            ; flag mask
        tstr    0c6h,0,0,0,009140h,07e3ch,07a67h,0df6dh,05b61h,00b29h,010h,066h,085b2h
        tstr    038h,0,0,0,0,0,0,0,0,0,0,-1,0           ; (2048 cycles)
        tstr    0,-1,0,0,0,0,0,0,0,0,0d7h,0,0           ; (14 cycles)
        defb    048h,079h,093h,060h                     ; expected crc
        defm    "aluop a,nn....................",0

; aluop a,<b,c,d,e,h,l,(hl),a> (753,664 cycles)
alu8r:  defb    0d7h            ; flag mask
        tstr    080h,0,0,0,0c53eh,0573ah,04c4dh,msbt,0e309h,0a666h,0d0h,03bh,0adbbh
        tstr    03fh,0,0,0,0,0,0,0,0,0,0,-1,0           ; (16,384 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,0,0       ; (46 cycles)
        defb    0feh,043h,0b0h,016h                     ; expected crc
        defm    "aluop a,<b,c,d,e,h,l,(hl),a>..",0

; aluop a,<ixh,ixl,iyh,iyl> (376,832 cycles)
alu8rx: defb    0d7h            ; flag mask
        tstr    0ddh,084h,0,0,0d6f7h,0c76eh,0accfh,02847h,022ddh,0c035h,0c5h,038h,0234bh
        tstr    020h,039h,0,0,0,0,0,0,0,0,0,-1,0        ; (8,192 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,0,0       ; (46 cycles)
        defb    0a4h,002h,06dh,05ah                     ; expected crc
        defm    "aluop a,<ixh,ixl,iyh,iyl>.....",0

; aluop a,(<ix,iy>+1) (229,376 cycles)
alu8x:  defb    0d7h            ; flag mask
        tstr    0ddh,086h,1,0,090b7h,msbt-1,msbt-1,032fdh,0406eh,0c1dch,045h,06eh,0e5fah
        tstr    020h,038h,0,0,0,1,1,0,0,0,0,-1,0        ; (16,384 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,0d7h,0,0         ; (14 cycles)
        defb    0e8h,049h,067h,06eh                     ; expected crc
        defm    "aluop a,(<ix,iy>+1)...........",0

; bit n,(<ix,iy>+1) (2048 cycles)
bitx:   defb    @01010001                               ; ZHC flag mask (was 053h)
        tstr    0ddh,0cbh,1,046h,02075h,msbt-1,msbt-1,03cfch,0a79ah,03d74h,051h,027h,0ca14h
        tstr    020h,0,0,038h,0,0,0,0,0,0,053h,0,0      ; (256 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,0,0,0            ; (8 cycles)
        defb    0a8h,0eeh,008h,067h                     ; expected crc
        defm    "bit n,(<ix,iy>+1).............",0

; bit n,<b,c,d,e,h,l,(hl),a> (49,152 cycles)
bitz80: defb    @01010001                               ; ZHC flag mask (was 053h)
        tstr    0cbh,040h,0,0,03ef1h,09dfch,07acch,msbt,0be61h,07a86h,050h,024h,01998h
        tstr    0,03fh,0,0,0,0,0,0,0,0,053h,0,0         ; (1024 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0,-1,0         ; (48 cycles)
        defb    07bh,055h,0e6h,0c8h                     ; expected crc
        defm    "bit n,<b,c,d,e,h,l,(hl),a>....",0

; cpd<r> (1) (6144 cycles)
cpd1:   defb    0d7h            ; flag mask
        tstr    0edh,0a9h,0,0,0c7b6h,072b4h,018f6h,msbt+17,08dbdh,1,0c0h,030h,094a3h
        tstr    0,010h,0,0,0,0,0,0,0,010,0,-1,0         ; (1024 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0a8h,07eh,06ch,0fah                     ; expected crc
        defm    "cpd<r>........................",0

; cpi<r> (1) (6144 cycles)
cpi1:   defb    0d7h            ; flag mask
        tstr    0edh,0a1h,0,0,04d48h,0af4ah,0906bh,msbt,04e71h,1,093h,06ah,0907ch
        tstr    0,010h,0,0,0,0,0,0,0,010,0,-1,0         ; (1024 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    006h,0deh,0b3h,056h                     ; expected crc
        defm    "cpi<r>........................",0

; <daa,cpl,scf,ccf>
daaop:  defb    0d7h            ; flag mask
        tstr    027h,0,0,0,02141h,009fah,01d60h,0a559h,08d5bh,09079h,004h,08eh,0299dh
        tstr    018h,0,0,0,0,0,0,0,0,0,0d7h,-1,0        ; (65,536 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    09bh,04bh,0a6h,075h                     ; expected crc
        defm    "<daa,cpl,scf,ccf>.............",0

; <inc,dec> a (3072 cycles)
inca:   defb    0d7h            ; flag mask
        tstr    03ch,0,0,0,04adfh,0d5d8h,0e598h,08a2bh,0a7b0h,0431bh,044h,05ah,0d030h
        tstr    001h,0,0,0,0,0,0,0,0,0,0,-1,0           ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0d1h,088h,015h,0a4h                     ; expected crc
        defm    "<inc,dec> a...................",0

; <inc,dec> b (3072 cycles)
incb:   defb    0d7h            ; flag mask
        tstr    004h,0,0,0,0d623h,0432dh,07a61h,08180h,05a86h,01e85h,086h,058h,09bbbh
        tstr    001h,0,0,0,0,0,0,0,0,0ff00h,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    05fh,068h,022h,064h                     ; expected crc
        defm    "<inc,dec> b...................",0

; <inc,dec> bc (1536 cycles)
incbc:  defb    0d7h            ; flag mask
        tstr    003h,0,0,0,0cd97h,044abh,08dc9h,0e3e3h,011cch,0e8a4h,002h,049h,02a4dh
        tstr    008h,0,0,0,0,0,0,0,0,0f821h,0,0,0       ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0d2h,0aeh,03bh,0ech                     ; expected crc
        defm    "<inc,dec> bc..................",0

; <inc,dec> c (3072 cycles)
incc:   defb    0d7h            ; flag mask
        tstr    00ch,0,0,0,0d789h,00935h,0055bh,09f85h,08b27h,0d208h,095h,005h,00660h
        tstr    001h,0,0,0,0,0,0,0,0,0ffh,0,0,0         ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0c2h,084h,055h,04ch                     ; expected crc
        defm    "<inc,dec> c...................",0

; <inc,dec> d (3072 cycles)
incd:   defb    0d7h            ; flag mask
        tstr    014h,0,0,0,0a0eah,05fbah,065fbh,0981ch,038cch,0debch,043h,05ch,003bdh
        tstr    001h,0,0,0,0,0,0,0,0ff00h,0,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    045h,023h,0deh,010h                     ; expected crc
        defm    "<inc,dec> d...................",0

; <inc,dec> de (1536 cycles)
incde:  defb    0d7h            ; flag mask
        tstr    013h,0,0,0,0342eh,0131dh,028c9h,00acah,09967h,03a2eh,092h,0f6h,09d54h
        tstr    008h,0,0,0,0,0,0,0,0f821h,0,0,0,0       ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0aeh,0c6h,0d4h,02ch                     ; expected crc
        defm    "<inc,dec> de..................",0

; <inc,dec> e (3072 cycles)
ince:   defb    0d7h            ; flag mask
        tstr    01ch,0,0,0,0602fh,04c0dh,02402h,0e2f5h,0a0f4h,0a10ah,013h,032h,05925h
        tstr    001h,0,0,0,0,0,0,0,0ffh,0,0,0,0         ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0e1h,075h,0afh,0cch                     ; expected crc
        defm    "<inc,dec> e...................",0

; <inc,dec> h (3072 cycles)
inch:   defb    0d7h            ; flag mask
        tstr    024h,0,0,0,01506h,0f2ebh,0e8ddh,0262bh,011a6h,0bc1ah,017h,006h,02818h
        tstr    001h,0,0,0,0,0,0,0ff00h,0,0,0,0,0       ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    01ch,0edh,084h,07dh                     ; expected crc
        defm    "<inc,dec> h...................",0

; <inc,dec> hl (1536 cycles)
inchl:  defb    0d7h            ; flag mask
        tstr    023h,0,0,0,0c3f4h,007a5h,01b6dh,04f04h,0e2c2h,0822ah,057h,0e0h,0c3e1h
        tstr    008h,0,0,0,0,0,0,0f821h,0,0,0,0,0       ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0fch,00dh,06dh,04ah                     ; expected crc
        defm    "<inc,dec> hl..................",0

; <inc,dec> ix (1536 cycles)
incix:  defb    0d7h            ; flag mask
        tstr    0ddh,023h,0,0,0bc3ch,00d9bh,0e081h,0adfdh,09a7fh,096e5h,013h,085h,00be2h
        tstr    0,8,0,0,0,0,0f821h,0,0,0,0,0,0          ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0a5h,04dh,0beh,031h                     ; expected crc
        defm    "<inc,dec> ix..................",0

; <inc,dec> iy (1536 cycles)
inciy:  defb    0d7h            ; flag mask
        tstr    0fdh,023h,0,0,09402h,0637ah,03182h,0c65ah,0b2e9h,0abb4h,016h,0f2h,06d05h
        tstr    0,8,0,0,0,0f821h,0,0,0,0,0,0,0          ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    050h,05dh,051h,0a3h                     ; expected crc
        defm    "<inc,dec> iy..................",0

; <inc,dec> l (3072 cycles)
incl:   defb    0d7h            ; flag mask
        tstr    02ch,0,0,0,08031h,0a520h,04356h,0b409h,0f4c1h,0dfa2h,0d1h,03ch,03ea2h
        tstr    001h,0,0,0,0,0,0,0ffh,0,0,0,0,0         ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    056h,0cdh,006h,0f3h                     ; expected crc
        defm    "<inc,dec> l...................",0

; <inc,dec> (hl) (3072 cycles)
incm:   defb    0d7h            ; flag mask
        tstr    034h,0,0,0,0b856h,00c7ch,0e53eh,msbt,0877eh,0da58h,015h,05ch,01f37h
        tstr    001h,0,0,0,0ffh,0,0,0,0,0,0,0,0         ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0b8h,03ah,0dch,0efh                     ; expected crc
        defm    "<inc,dec> (hl)................",0

; <inc,dec> sp (1536 cycles)
incsp:  defb    0d7h            ; flag mask
        tstr    033h,0,0,0,0346fh,0d482h,0d169h,0deb6h,0a494h,0f476h,053h,002h,0855bh
        tstr    008h,0,0,0,0,0,0,0,0,0,0,0,0f821h       ; (256 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    05dh,0ach,0d5h,027h                     ; expected crc
        defm    "<inc,dec> sp..................",0

; <inc,dec> (<ix,iy>+1) (6144 cycles)
incx:   defb    0d7h            ; flag mask
        tstr    0ddh,034h,1,0,0fa6eh,msbt-1,msbt-1,02c28h,08894h,05057h,016h,033h,0286fh
        tstr    020h,1,0,0,0ffh,0,0,0,0,0,0,0,0         ; (1024 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    020h,058h,014h,070h                     ; expected crc
        defm    "<inc,dec> (<ix,iy>+1).........",0

; <inc,dec> ixh (3072 cycles)
incxh:  defb    0d7h            ; flag mask
        tstr    0ddh,024h,0,0,0b838h,0316ch,0c6d4h,03e01h,08358h,015b4h,081h,0deh,04259h
        tstr    0,1,0,0,0,0ff00h,0,0,0,0,0,0,0          ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    06fh,046h,036h,062h                     ; expected crc
        defm    "<inc,dec> ixh.................",0

; <inc,dec> ixl (3072 cycles)
incxl:  defb    0d7h            ; flag mask
        tstr    0ddh,02ch,0,0,04d14h,07460h,076d4h,006e7h,032a2h,0213ch,0d6h,0d7h,099a5h
        tstr    0,1,0,0,0,0ffh,0,0,0,0,0,0,0            ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    002h,07bh,0efh,02ch                     ; expected crc
        defm    "<inc,dec> ixl.................",0

; <inc,dec> iyh (3072 cycles)
incyh:  defb    0d7h            ; flag mask
        tstr    0ddh,024h,0,0,02836h,09f6fh,09116h,061b9h,082cbh,0e219h,092h,073h,0a98ch
        tstr    0,1,0,0,0ff00h,0,0,0,0,0,0,0,0          ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    02dh,096h,06ch,0f3h                     ; expected crc
        defm    "<inc,dec> iyh.................",0

; <inc,dec> iyl (3072 cycles)
incyl:  defb    0d7h            ; flag mask
        tstr    0ddh,02ch,0,0,0d7c6h,062d5h,0a09eh,07039h,03e7eh,09f12h,090h,0d9h,0220fh
        tstr    0,1,0,0,0ffh,0,0,0,0,0,0,0,0            ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    0fbh,0cbh,0bah,095h                     ; expected crc
        defm    "<inc,dec> iyl.................",0

; ld <bc,de>,(nnnn) (32 cycles)
ld161:  defb    0d7h            ; flag mask
        tstr    0edh,04bh,msbtlo,msbthi,0f9a8h,0f559h,093a4h,0f5edh,06f96h,0d968h,086h,0e6h,04bd8h
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    04dh,045h,0a9h,0ach                     ; expected crc
        defm    "ld <bc,de>,(nnnn).............",0

; ld hl,(nnnn) (16 cycles)
ld162:  defb    0d7h            ; flag mask
        tstr    02ah,msbtlo,msbthi,0,09863h,07830h,02077h,0b1feh,0b9fah,0abb8h,004h,006h,06015h
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    05fh,097h,024h,087h                     ; expected crc
        defm    "ld hl,(nnnn)..................",0
        
; ld sp,(nnnn) (16 cycles)
ld163:  defb    0d7h            ; flag mask
        tstr    0edh,07bh,msbtlo,msbthi,08dfch,057d7h,02161h,0ca18h,0c185h,027dah,083h,01eh,0f460h
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    07ah,0ceh,0a1h,01bh                     ; expected crc
        defm    "ld sp,(nnnn)..................",0

; ld <ix,iy>,(nnnn) (32 cycles)
ld164:  defb    0d7h            ; flag mask
        tstr    0ddh,02ah,msbtlo,msbthi,0ded7h,0a6fah,0f780h,0244ch,087deh,0bcc2h,016h,063h,04c96h
        tstr    020h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    085h,08bh,0f1h,06dh                     ; expected crc
        defm    "ld <ix,iy>,(nnnn).............",0
        
; ld (nnnn),<bc,de> (64 cycles)
ld165:  defb    0d7h            ; flag mask
        tstr    0edh,043h,msbtlo,msbthi,01f98h,0844dh,0e8ach,0c9edh,0c95dh,08f61h,080h,03fh,0c7bfh
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,0,0,0,0,-1,-1,0,0,0             ; (32 cycles)
        defb    064h,01eh,087h,015h                     ; expected crc
        defm    "ld (nnnn),<bc,de>.............",0

; ld (nnnn),hl (16 cycles)
ld166:  defb    0d7h            ; flag mask
        tstr    022h,msbtlo,msbthi,0,0d003h,07772h,07f53h,03f72h,064eah,0e180h,010h,02dh,035e9h
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        tstr    0,0,0,0,0,0,0,-1,0,0,0,0,0              ; (16 cycles)
        defb    0a3h,060h,08bh,047h                     ; expected crc
        defm    "ld (nnnn),hl..................",0

; ld (nnnn),sp (16 cycles)
ld167:  defb    0d7h            ; flag mask
        tstr    0edh,073h,msbtlo,msbthi,0c0dch,0d1d6h,0ed5ah,0f356h,0afdah,06ca7h,044h,09fh,03f0ah
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,-1              ; (16 cycles)
        defb    016h,058h,05fh,0d7h                     ; expected crc
        defm    "ld (nnnn),sp..................",0

; ld (nnnn),<ix,iy> (64 cycles)
ld168:  defb    0d7h            ; flag mask
        tstr    0ddh,022h,msbtlo,msbthi,06cc3h,00d91h,06900h,08ef8h,0e3d6h,0c3f7h,0c6h,0d9h,0c2dfh
        tstr    020h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,0,-1,-1,0,0,0,0,0,0             ; (32 cycles)
        defb    0bah,010h,02ah,06bh                     ; expected crc
        defm    "ld (nnnn),<ix,iy>.............",0

; ld <bc,de,hl,sp>,nnnn (64 cycles)
ld16im: defb    0d7h            ; flag mask
        tstr    1,0,0,0,05c1ch,02d46h,08eb9h,06078h,074b1h,0b30eh,046h,0d1h,030cch
        tstr    030h,0,0,0,0,0,0,0,0,0,0,0,0            ; (4 cycles)
        tstr    0,0ffh,0ffh,0,0,0,0,0,0,0,0,0,0         ; (16 cycles)
        defb    0deh,039h,019h,069h                     ; expected crc
        defm    "ld <bc,de,hl,sp>,nnnn.........",0

; ld <ix,iy>,nnnn (32 cycles)
ld16ix: defb    0d7h            ; flag mask
        tstr    0ddh,021h,0,0,087e8h,02006h,0bd12h,0b69bh,07253h,0a1e5h,051h,013h,0f1bdh
        tstr    020h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0ffh,0ffh,0,0,0,0,0,0,0,0,0         ; (16 cycles)
        defb    022h,07dh,0d5h,025h                     ; expected crc
        defm    "ld <ix,iy>,nnnn...............",0

; ld a,<(bc),(de)> (44 cycles)
ld8bd:  defb    0d7h            ; flag mask
        tstr    00ah,0,0,0,0b3a8h,01d2ah,07f8eh,042ach,msbt,msbt,0c6h,0b1h,0ef8eh
        tstr    010h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,0d7h,-1,0        ; (22 cycles)
        defb    0b0h,081h,089h,035h                     ; expected crc
        defm    "ld a,<(bc),(de)>..............",0

; ld <b,c,d,e,h,l,(hl),a>,nn (64 cycles)
ld8im:  defb    0d7h            ; flag mask
        tstr    6,0,0,0,0c407h,0f49dh,0d13dh,00339h,0de89h,07455h,053h,0c0h,05509h
        tstr    038h,0,0,0,0,0,0,0,0,0,0,0,0            ; (8 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0,-1,0              ; (8 cycles)
        defb    0f1h,0dah,0b5h,056h                     ; expected crc
        defm    "ld <b,c,d,e,h,l,(hl),a>,nn....",0

; ld (<ix,iy>+1),nn (32 cycles)
ld8imx: defb    0d7h            ; flag mask
        tstr    0ddh,036h,1,0,01b45h,msbt-1,msbt-1,0d5c1h,061c7h,0bdc4h,0c0h,085h,0cd16h
        tstr    020h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,-1,0,0,0,0,0,0,0,-1,0             ; (16 cycles)
        defb    026h,0dbh,047h,07eh                     ; expected crc
        defm    "ld (<ix,iy>+1),nn.............",0

; ld <b,c,d,e>,(<ix,iy>+1) (512 cycles)
ld8ix1: defb    0d7h            ; flag mask
        tstr    0ddh,046h,1,0,0d016h,msbt-1,msbt-1,04260h,07f39h,00404h,097h,04ah,0d085h
        tstr    020h,018h,0,0,0,1,1,0,0,0,0,0,0         ; (32 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    0cch,011h,006h,0a8h                     ; expected crc
        defm    "ld <b,c,d,e>,(<ix,iy>+1)......",0

; ld <h,l>,(<ix,iy>+1) (256 cycles)
ld8ix2: defb    0d7h            ; flag mask
        tstr    0ddh,066h,1,0,084e0h,msbt-1,msbt-1,09c52h,0a799h,049b6h,093h,000h,0eeadh
        tstr    020h,008h,0,0,0,1,1,0,0,0,0,0,0         ; (16 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    0fah,02ah,04dh,003h                     ; expected crc
        defm    "ld <h,l>,(<ix,iy>+1)..........",0

; ld a,(<ix,iy>+1) (128 cycles)
ld8ix3: defb    0d7h            ; flag mask
        tstr    0ddh,07eh,1,0,0d8b6h,msbt-1,msbt-1,0c612h,0df07h,09cd0h,043h,0a6h,0a0e5h
        tstr    020h,0,0,0,0,1,1,0,0,0,0,0,0            ; (8 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,0,0              ; (16 cycles)
        defb    0a5h,0e9h,0ach,064h                     ; expected crc
        defm    "ld a,(<ix,iy>+1)..............",0

; ld <ixh,ixl,iyh,iyl>,nn (32 cycles)
ld8ixy: defb    0d7h            ; flag mask
        tstr    0ddh,026h,0,0,03c53h,04640h,0e179h,07711h,0c107h,01afah,081h,0adh,05d9bh
        tstr    020h,8,0,0,0,0,0,0,0,0,0,0,0            ; (4 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0,-1,0              ; (8 cycles)
        defb    024h,0e8h,082h,08bh                     ; expected crc
        defm    "ld <ixh,ixl,iyh,iyl>,nn.......",0

; ld <b,c,d,e,h,l,a>,<b,c,d,e,h,l,a> (3456 cycles)
ld8rr:  defb    0d7h            ; flag mask
        tstr    040h,0,0,0,072a4h,0a024h,061ach,msbt,082c7h,0718fh,097h,08fh,0ef8eh
        tstr    03fh,0,0,0,0,0,0,0,0,0,0,0,0            ; (64 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    074h,04bh,001h,018h                     ; expected crc
        defm    "ld <bcdehla>,<bcdehla>........",0

; ld <b,c,d,e,ixy,a>,<b,c,d,e,ixy,a> (6912 cycles)
ld8rrx: defb    0d7h            ; flag mask
        tstr    0ddh,040h,0,0,0bcc5h,msbt,msbt,msbt,02fc2h,098c0h,083h,01fh,03bcdh
        tstr    020h,03fh,0,0,0,0,0,0,0,0,0,0,0         ; (128 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (54 cycles)
        defb    047h,08bh,0a3h,06bh                     ; expected crc
        defm    "ld <bcdexya>,<bcdexya>........",0

; ld a,(nnnn) / ld (nnnn),a (44 cycles)
lda:    defb    0d7h            ; flag mask
        tstr    032h,msbtlo,msbthi,0,0fd68h,0f4ech,044a0h,0b543h,00653h,0cdbah,0d2h,04fh,01fd8h
        tstr    008h,0,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycle)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,0d7h,-1,0        ; (22 cycles)
        defb    0c9h,026h,02dh,0e5h                     ; expected crc
        defm    "ld a,(nnnn) / ld (nnnn),a.....",0

; ldd<r> (1) (44 cycles)
ldd1:   defb    0d7h            ; flag mask
        tstr    0edh,0a8h,0,0,09852h,068fah,066a1h,msbt+3,msbt+1,1,0c1h,068h,020b7h
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0d7h,0,0           ; (22 cycles)
        defb    094h,0f4h,027h,069h                     ; expected crc
        defm    "ldd<r> (1)....................",0

; ldd<r> (2) (44 cycles)
ldd2:   defb    0d7h            ; flag mask
        tstr    0edh,0a8h,0,0,0f12eh,0eb2ah,0d5bah,msbt+3,msbt+1,2,047h,0ffh,0fbe4h
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0d7h,0,0           ; (22 cycles)
        defb    05ah,090h,07eh,0d4h                     ; expected crc
        defm    "ldd<r> (2)....................",0

; ldi<r> (1) (44 cycles)
ldi1:   defb    0d7h            ; flag mask
        tstr    0edh,0a0h,0,0,0fe30h,003cdh,06058h,msbt+2,msbt,1,004h,060h,02688h
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0d7h,0,0           ; (22 cycles)
        defb    09ah,0bdh,0f6h,0b5h                     ; expected crc
        defm    "ldi<r> (1)....................",0

; ldi<r> (2) (44 cycles)
ldi2:   defb    0d7h            ; flag mask
        tstr    0edh,0a0h,0,0,04aceh,0c26eh,0b188h,msbt+2,msbt,2,014h,02dh,0a39fh
        tstr    0,010h,0,0,0,0,0,0,0,0,0,0,0            ; (2 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0d7h,0,0           ; (22 cycles)
        defb    0ebh,059h,089h,01bh                     ; expected crc
        defm    "ldi<r> (2)....................",0

; neg (16,384 cycles)
negop:  defb    0d7h            ; flag mask
        tstr    0edh,044h,0,0,038a2h,05f6bh,0d934h,057e4h,0d2d6h,04642h,043h,05ah,009cch
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0           ; (16,384 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0,0,0               ; (1 cycle)
        defb    06ah,03ch,03bh,0bdh                     ; expected crc
        defm    "neg...........................",0

; <rld,rrd> (7168 cycles)
rldop:  defb    0d7h            ; flag mask
        tstr    0edh,067h,0,0,091cbh,0c48bh,0fa62h,msbt,0e720h,0b479h,040h,006h,08ae2h
        tstr    0,8,0,0,0ffh,0,0,0,0,0,0,0,0            ; (512 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,-1,0           ; (14 cycles)
        defb    095h,05bh,0a3h,026h                     ; expected crc
        defm    "<rrd,rld>.....................",0

; <rlca,rrca,rla,rra> (6144 cycles)
rot8080: defb   0d7h            ; flag mask
        tstr    7,0,0,0,0cb92h,06d43h,00a90h,0c284h,00c53h,0f50eh,091h,0ebh,040fch
        tstr    018h,0,0,0,0,0,0,0,0,0,0,-1,0           ; (1024 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0d7h,0,0            ; (6 cycles)
        defb    025h,013h,030h,0aeh                     ; expected crc
        defm    "<rlca,rrca,rla,rra>...........",0

; shift/rotate (<ix,iy>+1) (416 cycles)
rotxy:  defb    0d7h            ; flag mask
        tstr    0ddh,0cbh,1,6,0ddafh,msbt-1,msbt-1,0ff3ch,0dbf6h,094f4h,082h,080h,061d9h
        tstr    020h,0,0,038h,0,0,0,0,0,0,080h,0,0      ; (32 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,057h,0,0         ; (13 cycles)
        defb    071h,03ah,0cdh,081h                     ; expected crc
        defm    "shf/rot (<ix,iy>+1)...........",0

; shift/rotate <b,c,d,e,h,l,(hl),a> (6784 cycles)
rotz80: defb    0d7h            ; flag mask
        tstr    0cbh,0,0,0,0ccebh,05d4ah,0e007h,msbt,01395h,030eeh,043h,078h,03dadh
        tstr    0,03fh,0,0,0,0,0,0,0,0,080h,0,0         ; (128 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,057h,-1,0      ; (53 cycles)
        defb    0ebh,060h,04dh,058h                     ; expected crc
        defm    "shf/rot <b,c,d,e,h,l,(hl),a>..",0

; <set,res> n,<b,c,d,e,h,l,(hl),a> (7936 cycles)
srz80:  defb    0d7h            ; flag mask
        tstr    0cbh,080h,0,0,02cd5h,097abh,039ffh,msbt,0d14bh,06ab2h,053h,027h,0b538h
        tstr    0,07fh,0,0,0,0,0,0,0,0,0,0,0            ; (128 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,-1,-1,0d7h,-1,0      ; (62 cycles)
        defb    08bh,057h,0f0h,008h                     ; expected crc
        defm    "<set,res> n,<bcdehl(hl)a>.....",0

; <set,res> n,(<ix,iy>+1) (1792 cycles)
srzx:   defb    0d7h            ; flag mask
        tstr    0ddh,0cbh,1,086h,0fb44h,msbt-1,msbt-1,0ba09h,068beh,032d8h,010h,05eh,0a867h
        tstr    020h,0,0,078h,0,0,0,0,0,0,0,0,0         ; (128 cycles)
        tstr    0,0,0,0,0ffh,0,0,0,0,0,0d7h,0,0         ; (14 cycles)
        defb    0cch,063h,0f9h,08ah                     ; expected crc
        defm    "<set,res> n,(<ix,iy>+1).......",0

; ld (<ix,iy>+1),<b,c,d,e> (1024 cycles)
st8ix1: defb    0d7h            ; flag mask
        tstr    0ddh,070h,1,0,0270dh,msbt-1,msbt-1,0b73ah,0887bh,099eeh,086h,070h,0ca07h
        tstr    020h,003h,0,0,0,1,1,0,0,0,0,0,0         ; (32 cycles)
        tstr    0,0,0,0,0,0,0,0,-1,-1,0,0,0             ; (32 cycles)
        defb    004h,062h,06ah,0bfh                     ; expected crc
        defm    "ld (<ix,iy>+1),<b,c,d,e>......",0

; ld (<ix,iy>+1),<h,l> (256 cycles)
st8ix2: defb    0d7h            ; flag mask
        tstr    0ddh,074h,1,0,0b664h,msbt-1,msbt-1,0e8ach,0b5f5h,0aafeh,012h,010h,09566h
        tstr    020h,001h,0,0,0,1,1,0,0,0,0,0,0         ; (16 cycles)
        tstr    0,0,0,0,0,0,0,-1,0,0,0,0,0              ; (32 cycles)
        defb    06ah,01ah,088h,031h                     ; expected crc
        defm    "ld (<ix,iy>+1),<h,l>..........",0

; ld (<ix,iy>+1),a (64 cycles)
st8ix3: defb    0d7h            ; flag mask
        tstr    0ddh,077h,1,0,067afh,msbt-1,msbt-1,04f13h,00644h,0bcd7h,050h,0ach,05fafh
        tstr    020h,0,0,0,0,1,1,0,0,0,0,0,0            ; (8 cycles)
        tstr    0,0,0,0,0,0,0,0,0,0,0,-1,0              ; (8 cycles)
        defb    0cch,0beh,05ah,096h                     ; expected crc
        defm    "ld (<ix,iy>+1),a..............",0

; ld (<bc,de>),a (96 cycles)
stabd:  defb    0d7h            ; flag mask
        tstr    2,0,0,0,00c3bh,0b592h,06cffh,0959eh,msbt,msbt+1,0c1h,021h,0bde7h
        tstr    018h,0,0,0,0,0,0,0,0,0,0,0,0            ; (4 cycles)
        tstr    0,0,0,0,-1,0,0,0,0,0,0,-1,0             ; (24 cycles)
        defb    07ah,04ch,011h,04fh                     ; expected crc
        defm    "ld (<bc,de>),a................",0

;       ----

;       start test pointed to by (hl)

stt:    push    hl
        ld      a,(hl)          ; get pointer to test
        inc     hl
        ld      h,(hl)
        ld      l,a
        ld      a,(hl)          ; flag mask
        ld      (flgmsk+1),a
        inc     hl
        push    hl
        ld      de,20
        add     hl,de           ; point to incmask
        ld      de,counter
        call    initmask
        pop     hl
        push    hl
        ld      de,20+20
        add     hl,de           ; point to scanmask
        ld      de,shifter
        call    initmask
        ld      hl,shifter
        ld      (hl),1          ; first bit
        pop     hl
        push    hl
        ld      de,iut          ; copy initial instruction under test
        ld      bc,4
        ldir
        ld      de,msbt         ; copy initial machine state
        ld      bc,16
        ldir
        ld      de,20+20+4      ; skip incmask, scanmask and expcrc
        add     hl,de
        ex      de,hl
        call    strout          ; show test name
        call    initcrc         ; initialise crc

;       test loop

tlp:    ld      a,(iut)
        cp      076h            ; pragmatically avoid halt intructions
        jp      z,tlp2
        and     a,0dfh
        cp      0ddh
        jp      nz,tlp1
        ld      a,(iut+1)
        cp      076h
tlp1:   call    nz,test         ; execute the test instruction
tlp2:   call    count           ; increment the counter
        call    nz,shift        ; shift the scan bit
        pop     hl              ; pointer to test case
        jp      z,tlp3          ; done if shift returned NZ
        ld      de,crcval
        call    crcout          ; display calculated CRC
        ld      de,20+20+20
        add     hl,de           ; point to expected crc
        call    cmpcrc          ; compare crc
        ld      de,okmsg
        jp      z,tlpok
        ld      de,nomsg
tlpok:  call    strout
        pop     hl
        inc     hl
        inc     hl
        ret

tlp3:   push    hl
        ld      a,1             ; initialise count and shift scanners
        ld      (cntbit),a
        ld      (shfbit),a
        ld      hl,counter
        ld      (cntbyt),hl
        ld      hl,shifter
        ld      (shfbyt),hl

        ld      b,4             ; bytes in iut field
        pop     hl              ; pointer to test case
        push    hl
        ld      de,iut
        call    setup           ; setup iut
        ld      b,16            ; bytes in machine state
        ld      de,msbt
        call    setup           ; setup machine state
        jp      tlp

;       ----

;       setup a field of the test case
;
; b  = number of bytes
; hl = pointer to base case
; de = destination

setup:  call    subyte
        inc     hl
        dec     b
        jp      nz,setup
        ret

subyte: push    bc
        push    de
        push    hl
        ld      c,(hl)          ; get base byte
        ld      de,20
        add     hl,de           ; point to incmask
        ld      a,(hl)
        cp      0
        jp      z,subshf
        ld      b,8             ; 8 bits
subclp: rrca
        push    af
        ld      a,0
        call    c,nxtcbit       ; get next counter bit if mask bit was set
        xor     c               ; flip bit if counter bit was set
        rrca
        ld      c,a
        pop     af
        dec     b
        jp      nz,subclp
        ld      b,8
subshf: ld      de,20
        add     hl,de           ; point to shift mask
        ld      a,(hl)
        cp      0
        jp      z,substr
        ld      b,8             ; 8 bits
sbshf1: rrca
        push    af
        ld      a,0
        call    c,nxtsbit       ; get next shifter bit if mask bit was set
        xor     c               ; flip bit if shifter bit was set
        rrca
        ld      c,a
        pop     af
        dec     b
        jp      nz,sbshf1
substr: pop     hl
        pop     de
        ld      a,c
        ld      (de),a          ; mangled byte to destination
        inc     de
        pop     bc
        ret

;       get next counter bit in low bit of a

cntbit: defs    1
cntbyt: defs    2

nxtcbit:push   bc
        push    hl
        ld      hl,(cntbyt)
        ld      b,(hl)
        ld      hl,cntbit
        ld      a,(hl)
        ld      c,a
        rlca
        ld      (hl),a
        cp      a,1
        jp      nz,ncb1
        ld      hl,(cntbyt)
        inc     hl
        ld      (cntbyt),hl
ncb1:   ld      a,b
        and     c
        pop     hl
        pop     bc
        ret     z
        ld      a,1
        ret
        
;       get next shifter bit in low bit of a

shfbit: defs    1
shfbyt: defs    2

nxtsbit: push   bc
        push    hl
        ld      hl,(shfbyt)
        ld      b,(hl)
        ld      hl,shfbit
        ld      a,(hl)
        ld      c,a
        rlca
        ld      (hl),a
        cp      a,1
        jp      nz,nsb1
        ld      hl,(shfbyt)
        inc     hl
        ld      (shfbyt),hl
nsb1:   ld      a,b
        and     c
        pop     hl
        pop     bc
        ret     z
        ld      a,1
        ret
        

;       clear memory at hl, bc bytes

clrmem: push    af
        push    bc
        push    de
        push    hl
        ld      (hl),0
        ld      d,h
        ld      e,l
        inc     de
        dec     bc
        ldir
        pop     hl
        pop     de
        pop     bc
        pop     af
        ret

;       initialise counter or shifter
;
; de = pointer to work area for counter or shifter
; hl = pointer to mask

initmask:
        push    de
        ex      de,hl
        ld      bc,20+20
        call    clrmem          ; clear work area
        ex      de,hl
        ld      b,20            ; byte counter
        ld      c,1             ; first bit
        ld      d,0             ; bit counter
imlp:   ld      e,(hl)
imlp1:  ld      a,e
        and     a,c
        jp      z,imlp2
        inc     d
imlp2:  ld      a,c
        rlca
        ld      c,a
        cp      a,1
        jp      nz,imlp1
        inc     hl
        dec     b
        jp      nz,imlp

;       got number of 1-bits in mask in reg d

        ld      a,d
        and     0f8h
        rrca
        rrca
        rrca                    ; divide by 8 (get byte offset)
        ld      l,a
        ld      h,0
        ld      a,d
        and     a,7             ; bit offset
        inc     a
        ld      b,a
        ld      a,080h
imlp3:  rlca
        dec     b
        jp      nz,imlp3
        pop     de
        add     hl,de
        ld      de,20
        add     hl,de
        ld      (hl),a
        ret

;       multi-byte counter

count:  push    bc
        push    de
        push    hl
        ld      hl,counter      ; 20 byte counter starts here
        ld      de,20           ; somewhere in here is the stop bit
        ex      de,hl
        add     hl,de
        ex      de,hl
cntlp:  inc     (hl)
        ld      a,(hl)
        cp      0
        jp      z,cntlp1        ; overflow to next byte
        ld      b,a
        ld      a,(de)
        and     a,b             ; test for terminal value
        jp      z,cntend
        ld      (hl),0          ; reset to zero
cntend: pop     bc
        pop     de
        pop     hl
        ret

cntlp1: inc     hl
        inc     de
        jp      cntlp
        

;       multi-byte shifter

shift:  push    bc
        push    de
        push    hl
        ld      hl,shifter      ; 20 byte shift register starts here
        ld      de,20           ; somewhere in here is the stop bit
        ex      de,hl
        add     hl,de
        ex      de,hl
shflp:  ld      a,(hl)
        or      a
        jp      z,shflp1
        ld      b,a
        ld      a,(de)
        and     b
        jp      nz,shlpe
        ld      a,b
        rlca
        cp      a,1
        jp      nz,shflp2
        ld      (hl),0
        inc     hl
        inc     de
shflp2: ld      (hl),a
        xor     a               ; set Z
shlpe:  pop     hl
        pop     de
        pop     bc
        ret
shflp1: inc     hl
        inc     de
        jp      shflp

counter: defs   2*20
shifter: defs   2*20

;       test harness

test:   push    af
        push    bc
        push    de
        push    hl
        ld      (spsav),sp      ; save stack pointer
        ld      sp,msbt+2       ; point to test-case machine state
        pop     iy              ; and load all regs
        pop     ix
        pop     hl
        pop     de
        pop     bc
        pop     af
        ld      sp,(spbt)
iut:    defs    4               ; max 4 byte instruction under test
        ld      (spat),sp       ; save stack pointer
        ld      sp,spat
        push    af              ; save other registers
        push    bc
        push    de
        push    hl
        push    ix
        push    iy
        ld      sp,(spsav)      ; restore stack pointer
        ld      hl,(msbt)       ; copy memory operand
        ld      (msat),hl
        ld      hl,flgsat       ; flags after test
        ld      a,(hl)
flgmsk: and     a,0d7h          ; mask-out irrelevant bits (self-modified code!)
        ld      (hl),a
        ld      b,16            ; total of 16 bytes of state
        ld      de,msat
        ld      hl,crcval
tcrc:   ld      a,(de)
        inc     de
        call    updcrc          ; accumulate crc of this test case
        dec     b
        jp      nz,tcrc
        pop     hl
        pop     de
        pop     bc
        pop     af
        ret

;       machine state after test

msat:   defs    14      ; memop,iy,ix,hl,de,bc,af
spat:   defs    2       ; stack pointer after test
        defc    flgsat = spat-2  ; flags
spsav:  defs    2       ; saved stack pointer

;       string output
;
; de = null terminated string

strout: push    af
        push    bc
soloop: ld      a, (de)
        or      a
        jr      z, soexit
        call    bytout
        inc     de
        jr      soloop
soexit: pop     bc
        pop     af
        ret

;       CRC32 hex output
;
; de = 32bit CRC

crcout: push    af
        push    bc
        ld      b, 4
crcolp:
        ld      a, (de)
        call    phex2
        inc     de
        djnz    crcolp
        pop     bc
        pop     af
        ret

; display byte in a
phex2:  push    af
        rrca
        rrca
        rrca
        rrca
        call    phex1
        pop     af

; display low nibble in a
phex1:  push    bc
        and     $0F
        cp      10
        jr      c,ph11
        add     a,'A'-'9'-1
ph11:   add     a,'0'
        call    bytout
        pop     bc
        ret

;       output byte
;
; a byte

bytout:
IF Z88
        push    af
.bowait
        in      a,($E5)                         ; UIT register
        and     @00010000                       ; TDRE, transmit data register empty
        jr      z,bowait
        ld      bc, @00000110 << 8 | $E3        ; 2 stops, 1 start | TXD register
        pop     af
        out     (c),a                   
        ret
ELSE
        ld      bc, $00FF
        out     (c),a
        ret
ENDIF

;       messages

msg1:   defm    13,10,"Z80doc instruction exerciser..CRC32",13,10,0
msg2:   defm    "Tests complete",13,10,0

okmsg:  defm    " OK",13,10,0
nomsg:  defm    " Error",13,10,0

;       compare crc
;
; hl points to value to compare to crcval

cmpcrc: push    bc
        push    de
        push    hl
        ld      de,crcval
        ld      b,4
cclp:   ld      a,(de)
        cp      a,(hl)
        jp      nz,cce
        inc     hl
        inc     de
        dec     b
        jp      nz,cclp
cce:    pop     hl
        pop     de
        pop     bc
        ret

;       32-bit crc routine
;
; entry: a contains next byte, hl points to crc
; exit:  crc updated

updcrc: push    af
        push    bc
        push    de
        push    hl
        push    hl
        ld      de,3
        add     hl,de   ; point to low byte of old crc
        xor     a,(hl)  ; xor with new byte
        ld      l,a
        ld      h,0
        add     hl,hl   ; use result as index into table of 4 byte entries
        add     hl,hl
        ex      de,hl
        ld      hl,crctab
        add     hl,de   ; point to selected entry in crctab
        ex      de,hl
        pop     hl
        ld      bc,4    ; c = byte count, b = accumulator
crclp:  ld      a,(de)
        xor     a,b
        ld      b,(hl)
        ld      (hl),a
        inc     de
        inc     hl
        dec     c
        jp      nz,crclp
        pop     hl
        pop     de
        pop     bc
        pop     af
        ret

initcrc:push    af
        push    bc
        push    hl
        ld      hl,crcval
        ld      a,0ffh
        ld      b,4
icrclp: ld      (hl),a
        inc     hl
        dec     b
        jp      nz,icrclp
        pop     hl
        pop     bc
        pop     af
        ret

crcval: defs    4

;       CRC32 table

crctab: defb    000h,000h,000h,000h
        defb    077h,007h,030h,096h
        defb    0eeh,00eh,061h,02ch
        defb    099h,009h,051h,0bah
        defb    007h,06dh,0c4h,019h
        defb    070h,06ah,0f4h,08fh
        defb    0e9h,063h,0a5h,035h
        defb    09eh,064h,095h,0a3h
        defb    00eh,0dbh,088h,032h
        defb    079h,0dch,0b8h,0a4h
        defb    0e0h,0d5h,0e9h,01eh
        defb    097h,0d2h,0d9h,088h
        defb    009h,0b6h,04ch,02bh
        defb    07eh,0b1h,07ch,0bdh
        defb    0e7h,0b8h,02dh,007h
        defb    090h,0bfh,01dh,091h
        defb    01dh,0b7h,010h,064h
        defb    06ah,0b0h,020h,0f2h
        defb    0f3h,0b9h,071h,048h
        defb    084h,0beh,041h,0deh
        defb    01ah,0dah,0d4h,07dh
        defb    06dh,0ddh,0e4h,0ebh
        defb    0f4h,0d4h,0b5h,051h
        defb    083h,0d3h,085h,0c7h
        defb    013h,06ch,098h,056h
        defb    064h,06bh,0a8h,0c0h
        defb    0fdh,062h,0f9h,07ah
        defb    08ah,065h,0c9h,0ech
        defb    014h,001h,05ch,04fh
        defb    063h,006h,06ch,0d9h
        defb    0fah,00fh,03dh,063h
        defb    08dh,008h,00dh,0f5h
        defb    03bh,06eh,020h,0c8h
        defb    04ch,069h,010h,05eh
        defb    0d5h,060h,041h,0e4h
        defb    0a2h,067h,071h,072h
        defb    03ch,003h,0e4h,0d1h
        defb    04bh,004h,0d4h,047h
        defb    0d2h,00dh,085h,0fdh
        defb    0a5h,00ah,0b5h,06bh
        defb    035h,0b5h,0a8h,0fah
        defb    042h,0b2h,098h,06ch
        defb    0dbh,0bbh,0c9h,0d6h
        defb    0ach,0bch,0f9h,040h
        defb    032h,0d8h,06ch,0e3h
        defb    045h,0dfh,05ch,075h
        defb    0dch,0d6h,00dh,0cfh
        defb    0abh,0d1h,03dh,059h
        defb    026h,0d9h,030h,0ach
        defb    051h,0deh,000h,03ah
        defb    0c8h,0d7h,051h,080h
        defb    0bfh,0d0h,061h,016h
        defb    021h,0b4h,0f4h,0b5h
        defb    056h,0b3h,0c4h,023h
        defb    0cfh,0bah,095h,099h
        defb    0b8h,0bdh,0a5h,00fh
        defb    028h,002h,0b8h,09eh
        defb    05fh,005h,088h,008h
        defb    0c6h,00ch,0d9h,0b2h
        defb    0b1h,00bh,0e9h,024h
        defb    02fh,06fh,07ch,087h
        defb    058h,068h,04ch,011h
        defb    0c1h,061h,01dh,0abh
        defb    0b6h,066h,02dh,03dh
        defb    076h,0dch,041h,090h
        defb    001h,0dbh,071h,006h
        defb    098h,0d2h,020h,0bch
        defb    0efh,0d5h,010h,02ah
        defb    071h,0b1h,085h,089h
        defb    006h,0b6h,0b5h,01fh
        defb    09fh,0bfh,0e4h,0a5h
        defb    0e8h,0b8h,0d4h,033h
        defb    078h,007h,0c9h,0a2h
        defb    00fh,000h,0f9h,034h
        defb    096h,009h,0a8h,08eh
        defb    0e1h,00eh,098h,018h
        defb    07fh,06ah,00dh,0bbh
        defb    008h,06dh,03dh,02dh
        defb    091h,064h,06ch,097h
        defb    0e6h,063h,05ch,001h
        defb    06bh,06bh,051h,0f4h
        defb    01ch,06ch,061h,062h
        defb    085h,065h,030h,0d8h
        defb    0f2h,062h,000h,04eh
        defb    06ch,006h,095h,0edh
        defb    01bh,001h,0a5h,07bh
        defb    082h,008h,0f4h,0c1h
        defb    0f5h,00fh,0c4h,057h
        defb    065h,0b0h,0d9h,0c6h
        defb    012h,0b7h,0e9h,050h
        defb    08bh,0beh,0b8h,0eah
        defb    0fch,0b9h,088h,07ch
        defb    062h,0ddh,01dh,0dfh
        defb    015h,0dah,02dh,049h
        defb    08ch,0d3h,07ch,0f3h
        defb    0fbh,0d4h,04ch,065h
        defb    04dh,0b2h,061h,058h
        defb    03ah,0b5h,051h,0ceh
        defb    0a3h,0bch,000h,074h
        defb    0d4h,0bbh,030h,0e2h
        defb    04ah,0dfh,0a5h,041h
        defb    03dh,0d8h,095h,0d7h
        defb    0a4h,0d1h,0c4h,06dh
        defb    0d3h,0d6h,0f4h,0fbh
        defb    043h,069h,0e9h,06ah
        defb    034h,06eh,0d9h,0fch
        defb    0adh,067h,088h,046h
        defb    0dah,060h,0b8h,0d0h
        defb    044h,004h,02dh,073h
        defb    033h,003h,01dh,0e5h
        defb    0aah,00ah,04ch,05fh
        defb    0ddh,00dh,07ch,0c9h
        defb    050h,005h,071h,03ch
        defb    027h,002h,041h,0aah
        defb    0beh,00bh,010h,010h
        defb    0c9h,00ch,020h,086h
        defb    057h,068h,0b5h,025h
        defb    020h,06fh,085h,0b3h
        defb    0b9h,066h,0d4h,009h
        defb    0ceh,061h,0e4h,09fh
        defb    05eh,0deh,0f9h,00eh
        defb    029h,0d9h,0c9h,098h
        defb    0b0h,0d0h,098h,022h
        defb    0c7h,0d7h,0a8h,0b4h
        defb    059h,0b3h,03dh,017h
        defb    02eh,0b4h,00dh,081h
        defb    0b7h,0bdh,05ch,03bh
        defb    0c0h,0bah,06ch,0adh
        defb    0edh,0b8h,083h,020h
        defb    09ah,0bfh,0b3h,0b6h
        defb    003h,0b6h,0e2h,00ch
        defb    074h,0b1h,0d2h,09ah
        defb    0eah,0d5h,047h,039h
        defb    09dh,0d2h,077h,0afh
        defb    004h,0dbh,026h,015h
        defb    073h,0dch,016h,083h
        defb    0e3h,063h,00bh,012h
        defb    094h,064h,03bh,084h
        defb    00dh,06dh,06ah,03eh
        defb    07ah,06ah,05ah,0a8h
        defb    0e4h,00eh,0cfh,00bh
        defb    093h,009h,0ffh,09dh
        defb    00ah,000h,0aeh,027h
        defb    07dh,007h,09eh,0b1h
        defb    0f0h,00fh,093h,044h
        defb    087h,008h,0a3h,0d2h
        defb    01eh,001h,0f2h,068h
        defb    069h,006h,0c2h,0feh
        defb    0f7h,062h,057h,05dh
        defb    080h,065h,067h,0cbh
        defb    019h,06ch,036h,071h
        defb    06eh,06bh,006h,0e7h
        defb    0feh,0d4h,01bh,076h
        defb    089h,0d3h,02bh,0e0h
        defb    010h,0dah,07ah,05ah
        defb    067h,0ddh,04ah,0cch
        defb    0f9h,0b9h,0dfh,06fh
        defb    08eh,0beh,0efh,0f9h
        defb    017h,0b7h,0beh,043h
        defb    060h,0b0h,08eh,0d5h
        defb    0d6h,0d6h,0a3h,0e8h
        defb    0a1h,0d1h,093h,07eh
        defb    038h,0d8h,0c2h,0c4h
        defb    04fh,0dfh,0f2h,052h
        defb    0d1h,0bbh,067h,0f1h
        defb    0a6h,0bch,057h,067h
        defb    03fh,0b5h,006h,0ddh
        defb    048h,0b2h,036h,04bh
        defb    0d8h,00dh,02bh,0dah
        defb    0afh,00ah,01bh,04ch
        defb    036h,003h,04ah,0f6h
        defb    041h,004h,07ah,060h
        defb    0dfh,060h,0efh,0c3h
        defb    0a8h,067h,0dfh,055h
        defb    031h,06eh,08eh,0efh
        defb    046h,069h,0beh,079h
        defb    0cbh,061h,0b3h,08ch
        defb    0bch,066h,083h,01ah
        defb    025h,06fh,0d2h,0a0h
        defb    052h,068h,0e2h,036h
        defb    0cch,00ch,077h,095h
        defb    0bbh,00bh,047h,003h
        defb    022h,002h,016h,0b9h
        defb    055h,005h,026h,02fh
        defb    0c5h,0bah,03bh,0beh
        defb    0b2h,0bdh,00bh,028h
        defb    02bh,0b4h,05ah,092h
        defb    05ch,0b3h,06ah,004h
        defb    0c2h,0d7h,0ffh,0a7h
        defb    0b5h,0d0h,0cfh,031h
        defb    02ch,0d9h,09eh,08bh
        defb    05bh,0deh,0aeh,01dh
        defb    09bh,064h,0c2h,0b0h
        defb    0ech,063h,0f2h,026h
        defb    075h,06ah,0a3h,09ch
        defb    002h,06dh,093h,00ah
        defb    09ch,009h,006h,0a9h
        defb    0ebh,00eh,036h,03fh
        defb    072h,007h,067h,085h
        defb    005h,000h,057h,013h
        defb    095h,0bfh,04ah,082h
        defb    0e2h,0b8h,07ah,014h
        defb    07bh,0b1h,02bh,0aeh
        defb    00ch,0b6h,01bh,038h
        defb    092h,0d2h,08eh,09bh
        defb    0e5h,0d5h,0beh,00dh
        defb    07ch,0dch,0efh,0b7h
        defb    00bh,0dbh,0dfh,021h
        defb    086h,0d3h,0d2h,0d4h
        defb    0f1h,0d4h,0e2h,042h
        defb    068h,0ddh,0b3h,0f8h
        defb    01fh,0dah,083h,06eh
        defb    081h,0beh,016h,0cdh
        defb    0f6h,0b9h,026h,05bh
        defb    06fh,0b0h,077h,0e1h
        defb    018h,0b7h,047h,077h
        defb    088h,008h,05ah,0e6h
        defb    0ffh,00fh,06ah,070h
        defb    066h,006h,03bh,0cah
        defb    011h,001h,00bh,05ch
        defb    08fh,065h,09eh,0ffh
        defb    0f8h,062h,0aeh,069h
        defb    061h,06bh,0ffh,0d3h
        defb    016h,06ch,0cfh,045h
        defb    0a0h,00ah,0e2h,078h
        defb    0d7h,00dh,0d2h,0eeh
        defb    04eh,004h,083h,054h
        defb    039h,003h,0b3h,0c2h
        defb    0a7h,067h,026h,061h
        defb    0d0h,060h,016h,0f7h
        defb    049h,069h,047h,04dh
        defb    03eh,06eh,077h,0dbh
        defb    0aeh,0d1h,06ah,04ah
        defb    0d9h,0d6h,05ah,0dch
        defb    040h,0dfh,00bh,066h
        defb    037h,0d8h,03bh,0f0h
        defb    0a9h,0bch,0aeh,053h
        defb    0deh,0bbh,09eh,0c5h
        defb    047h,0b2h,0cfh,07fh
        defb    030h,0b5h,0ffh,0e9h
        defb    0bdh,0bdh,0f2h,01ch
        defb    0cah,0bah,0c2h,08ah
        defb    053h,0b3h,093h,030h
        defb    024h,0b4h,0a3h,0a6h
        defb    0bah,0d0h,036h,005h
        defb    0cdh,0d7h,006h,093h
        defb    054h,0deh,057h,029h
        defb    023h,0d9h,067h,0bfh
        defb    0b3h,066h,07ah,02eh
        defb    0c4h,061h,04ah,0b8h
        defb    05dh,068h,01bh,002h
        defb    02ah,06fh,02bh,094h
        defb    0b4h,00bh,0beh,037h
        defb    0c3h,00ch,08eh,0a1h
        defb    05ah,005h,0dfh,01bh
        defb    02dh,002h,0efh,08dh

