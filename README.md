# ZEXDOC

ZEXDOC port for minimal Z80 simulator and Z88 platform. 

ZEXDOC is a Z80 CPU exerciser for the documented instruction (bits 3 and 5 of F register are ignored). It was written by Frank D. Cringle in 1994.

This port has been written to verify Z80 CPU behaviour during HDL design. 


## Binary files :

**zexdoc.bin**, for bare CPU+64K RAM, copy at 0000h

**zexdoc88.bin**, bank 0 rom binary for Z88 platform

**zexdoc88.rom**, 128K Z88 ROM with OZ header for emulator

During execution, output message are written in RAM starting at 3000h (203000h on Z88 platform).

## Compilation :

[MPM assembler](https://gitlab.com/bits4fun/mpm) and [Z88CARD tool](https://gitlab.com/z88/z88card) are required.

Run *./zex.sh* script.
